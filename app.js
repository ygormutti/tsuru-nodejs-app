'use strict';

let express = require('express');
let uuid = require('node-uuid');

let app = express();
let id = uuid.v4();
let startedAt = new Date();

app.get('/', (req, res) => {
  res.send({ id, startedAt });
});

let port = process.env.PORT || 3000
app.listen(port, () => console.log('tsuru-nodejs-app is listening on port ' + port));