# tsuru-nodejs-app

This is a tsuru app for NodeJS platform created to show some of tsuru PaaS capabilities. It's a HTTP service made using Express.

## Valid requests

### GET /

Returns a JSON object containing the following fields:
 * `id` - string that uniquely identifies this app instance
 * `startedAt` - string represening date and time when this app instance was started

## Installing

To install the app dependendies, `cd` to the app root and run:

```
npm install
```

## Running

To start the service after installing run:

```
npm start
```

## tsuru users

When running in a tsuru environment, `tsuru.yaml` is responsible for installing and `Procfile` is responsible for running.

### If you're new to tsuru

Read the [overview](https://docs.tsuru.io/stable/understanding/overview.html) and [concepts](https://docs.tsuru.io/stable/understanding/concepts.html) sections of tsuru's documentation. It won't take more than 10 minutes and is required to understand the next steps.

#### Installing tsuru

You can easily do a toy installation of tsuru (not for production environments) by using vagrant and [tsuru-bootstrap](https://github.com/tsuru/tsuru-bootstrap).

Use your system package manager to install Vagrant and then check the README in the link above for instructions to install tsuru-bootstrap.

At its end, the installation script will print the tsuru API target URL and the username and password of the admin user, which are required for the next steps.

#### Installing clients

To setup your tsuru installation and apps you'll need to install [tsuru CLI client](https://github.com/tsuru/tsuru-client/releases) and [tsuru-admin](https://github.com/tsuru/tsuru-admin/releases). There's a PPA available for Ubuntu users.

After installing you'll need to configure the client to access your tsuru installation through the target URL. Type these commands in a terminal to learn how to do this:

```
tsuru target-add 
tsuru target-set
```

After adding the target and setting it as the current target, login by giving the credentials of the admin user to the login command:

```
tsuru login
```

#### Adding platform

tsuru-bootstrap comes only with Python platform preinstalled, so you will have to add NodeJS platform to run this app by running:

```
tsuru-admin platform-add nodejs -d https://raw.github.com/tsuru/basebuilder/master/nodejs/Dockerfile
```

After this command completes, you'll be able to register this app and deploy it:  

```
tsuru app-create tsuru-nodejs-app nodejs -t admin
tsuru app-deploy --app tsuru-nodejs-app .
```

After the deploy ends run the next command to check the app address:

```
tsuru app-info --app tsuru-nodejs-app
```

If you copy and paste the address to your browser it should display the JSON specified above. You can also enable replication and load-balancing simply by adding more units to it:

```
tsuru unit-add 2 --app tsuru-nodejs-app
```

This will add two new units to the app. Now, if you refresh that address opened in your browser you will be able to see that tsuru's router will deliver each request to a different unit (instance).